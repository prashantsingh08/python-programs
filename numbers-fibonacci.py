def fibonacci(limit):
    a = 0
    b = 1
    for _ in range(limit):
        yield a
        a, b = b, a+b


limit = int(input('Enter the limiting number for fibonacci sequence: '))
print('Fibonacci series upto {} is:'.format(limit))

for num in fibonacci(limit):
    print(num, end=' ')
    