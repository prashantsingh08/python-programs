import math

value = float(input('Enter the e precision you want: '))

if value < 52 and value >= 0:
    value = int(value)
    print('Value of e upto {} precision is {:.{}f}'.format(value, math.e, value))

else:
    print('Enter any non negative number less than 52')