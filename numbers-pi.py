import math

value = float(input('Enter the pi precision you want: '))

if value < 49 and value >= 0:
    value = int(value)
    print('Value of pi upto {} precision is {:.{}f}'.format(value, math.pi, value))

else:
    print('Enter any non negative number less than 52')